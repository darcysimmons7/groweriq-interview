import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];
  gender: string;
  userType: string;
  greenhouses: string[];

  constructor(private userService: UserService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.userService.getUsers().subscribe(users => this.users = users); // asynchronous call
  }

  addWHEmployee(
    name: string,
    address: string,
    phoneNumber: string,
    gender: string,
    experience: number,
    education: string,
    certification: string
  ): void {
    name = name.trim();
    if (!name) { return; }
    const newUser: User = {
      name,
      address,
      phoneNumber,
      gender,
      work: 'WarehouseEmployee',
      experience,
      education,
      certification
    };
    this.userService.addUser(newUser)
      .subscribe(user => {
        this.users.push(user);
      });
  }
  addGrower(
    name: string,
    phoneNumber: string,
    address: string,
    gender: string,
    batches: number,
    yields: number,
    greenhouses: string[]
  ): void {
    name = name.trim();
    if (!name) { return; }
    const newUser: User = {
      name,
      address,
      phoneNumber,
      gender,
      work: 'Grower',
      batches,
      yield: yields,
      greenhouses
    };
    this.userService.addUser(newUser)
    .subscribe(user => {
      this.users.push(user);
    });
  }

}


