export interface User {
    phoneNumber: string;
    name: string;
    work: string;
    gender: string;
    address: string;
    batches?: number ;
    yield?: number ;
    greenhouses?: string[];
    experience?: number;
    education?: string;
    certification?: string;
    id?: number;
  }
