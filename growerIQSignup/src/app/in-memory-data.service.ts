import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { User } from './user';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const users = [
      { phoneNumber: '234-234-2345', name: 'Lisa Luna', work: 'Grower', id: 1 , address: '12 True Lane',
        gender: 'Female', batches: 120, yield: 300, greenhouses: ['Toronto', 'Hamilton']},
      { phoneNumber: '345-345-3456', name: 'Tommy Ticker', work: 'Grower', id: 2, address: '62 Louis Street',
      gender: 'Male', batches: 67, yield: 233, greenhouses: ['Burlington', 'Guelph']},
      { phoneNumber: '456-456-4567', name: 'Jack Jones', work: 'Warehouse Employee' , id: 3, address: '987 Salt Street',
      gender: 'Female', experience: 3, education: 'bachelors', certification: 'yes'},
      { phoneNumber: '567-567-5678', name: 'Cara Cranston' , work: 'Grower' , id: 4, address: '152 James Parkway',
      gender: 'Other', batches: 54, yield: 209, greenhouses: ['Cambridge']},
      { phoneNumber: '678-678-6789', name: 'Nicky Nixon', work: 'Warehouse Employee' , id: 5, address: '47 Truman Road',
      gender: 'Male', experience: 2, education: 'masters', certification: 'yes'},
      { phoneNumber: '789-789-7890', name: 'Robert Rhodes', work: 'Warehouse Employee' , id: 6 , address: '321 Pauper Crescent',
      gender: 'Other', experience: 7, education: 'bachelors', certification: 'yes'},
      { phoneNumber: '890-890-8901', name: 'Darcy Drake' , work: 'Grower', id: 7, address: '98 Lockhart Street',
      gender: 'Female', batches: 79, yield: 200, greenhouses: ['Toronto', 'Guelph', 'Kitchener'] },
      { phoneNumber: '901-901-9012', name: 'Sally Smith' , work: 'Warehouse Employee', id: 8 , address: '388 Cat Lane',
      gender: 'Female', experience: 4, education: 'PHD', certification: 'yes'}
    ];
    return {users};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(users: User[]): number {
    return users.length > 0 ? Math.max(...users.map(user => user.id)) + 1 : 11;
  }
}
